var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST home page. */
router.post('/', function(req, res, next) {
  console.log(JSON.stringify(req.body, null, 2));
  
  res.status(200).send();
});

module.exports = router;
