let express = require('express');
let router = express.Router();
// line sdk
const line = require('@line/bot-sdk');
// line bot channel access token
const channelAccessToken = '<channelAccessToken>';
// init a client 
const client = new line.Client({
	channelAccessToken: channelAccessToken
});

router.post('/line', async function(req, res, next) {

	console.log(JSON.stringify(req.body, null, 2));
	console.log('webhook received an event');
    res.status(200).send();
	
	let targetEvent = req.body.events[0];
	if(targetEvent.type == 'message'){
		replyToLine(targetEvent.replyToken, targetEvent.message);
	}
});

module.exports = router;


function replyToLine(rplyToken, message) {

	return client.replyMessage(rplyToken, message)
	.then((res) => {
		console.log(res)
		return true; 
	})
	.catch((err) => {
		console.log(err)
		return false;
	});
}